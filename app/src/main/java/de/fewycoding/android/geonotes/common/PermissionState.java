package de.fewycoding.android.geonotes.common;

public enum PermissionState {
    GRANTED, PERMISSION_DECLINED, PERMISSION_DONT_ASK_AGAIN
}
