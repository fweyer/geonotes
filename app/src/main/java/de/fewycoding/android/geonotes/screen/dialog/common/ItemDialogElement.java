package de.fewycoding.android.geonotes.screen.dialog.common;

public interface ItemDialogElement {

    String getDiscription();
}
