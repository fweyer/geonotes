package de.fewycoding.android.geonotes.screen.dialog.common;

import android.content.DialogInterface;

import java.util.List;

public interface ItemClick<Item> {

    void action (DialogInterface dialog, int which, List<Item> items);
}
