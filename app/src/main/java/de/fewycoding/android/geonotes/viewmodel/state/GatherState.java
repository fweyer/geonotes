package de.fewycoding.android.geonotes.viewmodel.state;

public enum GatherState {
    AKTUELLES_PROJEKT_NULL,
    AKTUELLE_NOTIZ_NULL,
    AKTUELLES_PROJEKT_VERAENDERT,
    AKTUELLES_NOTIZ_VERAENDERT

}
