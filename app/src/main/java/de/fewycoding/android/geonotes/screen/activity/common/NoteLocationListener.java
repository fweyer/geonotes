package de.fewycoding.android.geonotes.screen.activity.common;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.util.Log;

public class NoteLocationListener implements LocationListener {
    @Override
    public void onLocationChanged(Location location) {
        Log.d(getClass().getSimpleName(), "Empfangene Geodaten:\n" + location.toString());
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }
}
