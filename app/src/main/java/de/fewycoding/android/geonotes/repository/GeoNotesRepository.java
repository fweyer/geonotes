package de.fewycoding.android.geonotes.repository;

import android.content.ContentValues;
import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import de.fewycoding.android.geonotes.App;
import de.fewycoding.android.geonotes.database.GeoNotesDatabaseHelper;
import de.fewycoding.android.geonotes.database.entity.Notiz;
import de.fewycoding.android.geonotes.database.entity.Projekt;

public class GeoNotesRepository {

    public static GeoNotesRepository instance;
    private final GeoNotesDatabaseHelper helper;

    private GeoNotesRepository(Context context){
        this.helper = new GeoNotesDatabaseHelper(context);
    }

    public static GeoNotesRepository getInstance(){
        if (instance == null){
            instance = new GeoNotesRepository(App.getContext());
        }
        return instance;
    }

    public Projekt findProjektByID(long id){
        for(Projekt projekt : getProjekte()){
            if (projekt.id == id){
                return projekt;
            }
        }
        return null;
    }

    public void insertProjekt(ContentValues values){
        helper.insert("Projekte", values);
    }

    public List<Projekt> getProjekte() {
        return helper.getProjekte();
    }

    public void updateProjekt(ContentValues values){
        helper.update("Projekte", values);
    }

    public void insertNotizen(ContentValues values){
        helper.insert("Notizen", values);
    }

    public void updateNotizen(ContentValues values){
        helper.update("Notizen", values);
    }

    public Notiz getVorherigeNotiz(Notiz notiz) {
       return helper.getVorherigeNotiz(notiz);
    }

    public Notiz getNaechsteNotiz(Notiz notiz) {
        return helper.getNaechsteNotiz(notiz);
    }

    public Notiz getLetzteNotiz(Projekt projekt) {
        return helper.getLetzteNotiz(projekt);
    }

    public ArrayList<Notiz> getNotizen(Projekt projekt) {
       return helper.getNotizen(projekt);
    }

    public void deleteNotiz(Notiz notiz) {
        helper.deleteNotiz("Notizen", notiz);
    }

    public void deleteProjekt(Projekt projekt){
        helper.deleteProjekt("Projekte", projekt);
    }

    public void insertLocation(ContentValues values){ helper.insert("Locations", values);}


}
