package de.fewycoding.android.geonotes.viewmodel;

import android.location.Location;

import com.google.android.gms.common.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

import de.fewycoding.android.geonotes.database.entity.LocationEntity;
import de.fewycoding.android.geonotes.database.entity.Notiz;
import de.fewycoding.android.geonotes.database.entity.Projekt;
import de.fewycoding.android.geonotes.repository.GeoNotesRepository;
import de.fewycoding.android.geonotes.viewmodel.common.ObservableModel;
import de.fewycoding.android.geonotes.viewmodel.state.GatherState;

public class GatherViewModel extends ObservableModel<GatherState> {

    private static GatherViewModel instance;

    // Zugriff Klassen
    private final GeoNotesRepository repository;

    //Daten Klassen
    private Projekt aktuellesProjekt;
    private Notiz aktuelleNotiz;
    private int minTime = 9000; // in Millisekunden
    private int minDistance = 10; // in Metern


    private GatherViewModel(){
        this.repository = GeoNotesRepository.getInstance();
    }

    public static GatherViewModel getInstance(){
        if (instance == null){
            instance = new GatherViewModel();
            instance.setMemberObserverAllowed(1);
        }
        return instance;
    }

    public void setAktuellesProjekt(long aktuellesProjektId) {
        this.aktuellesProjekt = repository.findProjektByID(aktuellesProjektId);
        if (aktuellesProjekt == null) {
            notifyUpdates(GatherState.AKTUELLES_PROJEKT_NULL);
            return;
        }
        notifyUpdates(GatherState.AKTUELLES_PROJEKT_VERAENDERT);
        aktuelleNotiz =  repository.getLetzteNotiz(aktuellesProjekt);
        if(aktuelleNotiz == null){
            notifyUpdates(GatherState.AKTUELLE_NOTIZ_NULL);
        } else {
            notifyUpdates(GatherState.AKTUELLES_NOTIZ_VERAENDERT);
        }
    }

    public void projektSpeichern(String beschreibung, boolean updateProjekt){
        if (updateProjekt) {
            aktuellesProjekt.setBeschreibung(beschreibung);
            repository.updateProjekt(aktuellesProjekt.getContentValues());
        } else {
            aktuellesProjekt = new Projekt(System.currentTimeMillis(), beschreibung);
            repository.insertProjekt(aktuellesProjekt.getContentValues());
        }
        notifyUpdates(GatherState.AKTUELLES_PROJEKT_VERAENDERT);
    }

    public void deleteProjekt(){
        repository.deleteProjekt(aktuellesProjekt);
        notifyUpdates(GatherState.AKTUELLES_PROJEKT_NULL);
    }

    public void checkAktuellProjekt(){
        if (isProjektListeLeer()) {
            notifyUpdates(GatherState.AKTUELLES_PROJEKT_NULL);
        }
    }

    public boolean deleteAktuelleNotiz(){
        if (aktuelleNotiz == null) return false;
        int indexZuLoeschendeNotiz = getAktuelleNotizList().indexOf(aktuelleNotiz);
        repository.deleteNotiz(aktuelleNotiz);
        if (!CollectionUtils.isEmpty(getAktuelleNotizList())) {
            aktuelleNotizNeuSetzen(indexZuLoeschendeNotiz);
            return true;
        } else {
            aktuelleNotiz = null;
            notifyUpdates(GatherState.AKTUELLE_NOTIZ_NULL);
            return false;
        }
    }

    public void notizSpeichern(String thema, String notiz, Location lastLocation, String provider){
        repository.insertProjekt(aktuellesProjekt.getContentValues());
        if (aktuelleNotiz == null) {
            LocationEntity location = new LocationEntity(lastLocation.getLatitude(),
                    lastLocation.getLongitude(), (int) lastLocation.getAltitude(), provider);

            repository.insertLocation(location.getContentValues());
            aktuelleNotiz = new Notiz(aktuellesProjekt.id,
                    lastLocation.getLatitude(),
                    lastLocation.getLongitude(),
                    thema, notiz);

            repository.insertNotizen(aktuelleNotiz.getContentValues());
            aktuelleNotiz = null;
            notifyUpdates(GatherState.AKTUELLE_NOTIZ_NULL);
        } else {
            aktuelleNotiz.setThema(thema);
            aktuelleNotiz.setNotiz(notiz);
            repository.updateNotizen(aktuelleNotiz.getContentValues());
            notifyUpdates(GatherState.AKTUELLES_NOTIZ_VERAENDERT);
        }
    }

    public void voherigeNotiz() {
        if(CollectionUtils.isEmpty(getAktuelleNotizList())) return;
        if (aktuelleNotiz == null){
            aktuelleNotiz = repository.getLetzteNotiz(aktuellesProjekt);
        } else {
            aktuelleNotiz = repository.getVorherigeNotiz(aktuelleNotiz);
        }
        notifyUpdates(GatherState.AKTUELLES_NOTIZ_VERAENDERT);
    }

    public void nachsteNotiz() {
        if (aktuelleNotiz == null) return;
        aktuelleNotiz = repository.getNaechsteNotiz(aktuelleNotiz);
        if (aktuelleNotiz == null) {
            notifyUpdates(GatherState.AKTUELLE_NOTIZ_NULL);
        } else {
           notifyUpdates(GatherState.AKTUELLES_NOTIZ_VERAENDERT);
        }
    }

    private void aktuelleNotizNeuSetzen(int index){
        if(index > 0){
            voherigeNotiz();
        } else {
            aktuelleNotiz = getAktuelleNotizList().get(0);
            notifyUpdates(GatherState.AKTUELLES_NOTIZ_VERAENDERT);
        }
    }

    public Projekt getAktuellesProjekt() {
        return aktuellesProjekt;
    }

    public Boolean isProjektListeLeer(){
        return (getProjektList().size() <= 0);
    }

    public Boolean isNotizListeLeer(){
        return repository.getNotizen(aktuellesProjekt).size() <= 0;
    }

    public List<Projekt> getProjektList(){
        return repository.getProjekte();
    }

    public ArrayList<Notiz> getAktuelleNotizList(){
        return repository.getNotizen(aktuellesProjekt);
    }

    public String getAktuellenProjektTitel(){
        return aktuellesProjekt.getBeschreibung();
    }

    public Notiz getAktuelleNotiz() {
        return aktuelleNotiz;
    }

    public int getMinTime() {
        return minTime;
    }

    public void setMinTime(int minTime) {
        this.minTime = minTime;
    }

    public int getMinDistance() {
        return minDistance;
    }

    public void setMinDistance(int minDistance) {
        this.minDistance = minDistance;
    }



}
