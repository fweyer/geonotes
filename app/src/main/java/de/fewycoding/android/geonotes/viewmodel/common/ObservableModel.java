package de.fewycoding.android.geonotes.viewmodel.common;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class ObservableModel<UPDATE_EVENT> {

    private final List<ObserverView> mObservers = new ArrayList<>();
    private int membersObserverAllowed = 5;

    public void registerObserver(ObserverView observerView){
        if (mObservers.size() > membersObserverAllowed){
            throw new RuntimeException("There are more than "+ membersObserverAllowed+" Observers in the List. " +
                    "Please check potential memoryleaks");
        }else {
            this.mObservers.add(observerView);
        }
    };

    public void unregisterObserver(ObserverView observerView){
        this.mObservers.remove(observerView);
    }

    protected void notifyUpdates(UPDATE_EVENT event){
        for (ObserverView observer : mObservers){
            observer.update(event);
        }
    }

    protected final List<ObserverView> getmObservers() {
        return Collections.unmodifiableList(mObservers);
    }


    protected int getMemberObserverAllowed() {
        return membersObserverAllowed;
    }

    protected void setMemberObserverAllowed(int memberObserverAllowed) {
        this.membersObserverAllowed = memberObserverAllowed;
    }


}
