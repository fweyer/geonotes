package de.fewycoding.android.geonotes.database.entity;

import android.content.ContentValues;

public class LocationEntity {

    public final double latitude;
    public final double longitude;
    public final int altitude;
    public final String provider;

    public LocationEntity(double latitude, double longitude, int altitude, String provider) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
        this.provider = provider;
    }

    public ContentValues getContentValues() {
        ContentValues values = new ContentValues(4);
        values.put("latitude", latitude);
        values.put("longitude", longitude);
        values.put("altitude", altitude);
        values.put("provider", provider);
        return values;
    }

}
