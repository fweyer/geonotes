package de.fewycoding.android.geonotes.database.entity;

import android.content.ContentValues;
import android.database.Cursor;

import de.fewycoding.android.geonotes.screen.dialog.common.ItemDialogElement;

public class Projekt implements ItemDialogElement {

    public final long id;
    private String beschreibung;

    public Projekt(Cursor cursor) {
        this(cursor.getLong(cursor.getColumnIndex("id")),
                cursor.getString(cursor.getColumnIndex("beschreibung")));
    }

    public Projekt(long id, String beschreibung) {
        this.id = id;
        this.beschreibung = beschreibung;
    }

    public String getBeschreibung() {
        return beschreibung;
    }

    public void setBeschreibung(String beschreibung) {
        this.beschreibung = beschreibung;
    }

    public ContentValues getContentValues() {
        ContentValues values = new ContentValues(2);
        values.put("id", id);
        values.put("beschreibung", beschreibung);
        return values;
    }

    @Override
    public String toString() {
        return beschreibung;
    }


    @Override
    public String getDiscription() {
        return beschreibung;
    }
}
