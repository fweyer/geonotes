package de.fewycoding.android.geonotes.screen.dialog.components;

import android.app.AlertDialog;
import android.view.View;

import de.fewycoding.android.geonotes.App;
import de.fewycoding.android.geonotes.screen.dialog.common.CancelButtonClick;
import de.fewycoding.android.geonotes.screen.dialog.common.NegativButtonClick;
import de.fewycoding.android.geonotes.screen.dialog.common.PositivButtonClick;

public class PromtDialog {


    AlertDialog.Builder dialog;

    public PromtDialog(
            String title,
            String positivButtonDiscription,
            String negativButtonDiscription,
            PositivButtonClick positivButtonClick,
            NegativButtonClick negativButtonClick
    )
    {
        dialog = new AlertDialog.Builder(App.getContext());
        dialog.setTitle(title);
        dialog.setNegativeButton(negativButtonDiscription, negativButtonClick::action);
        dialog.setPositiveButton(positivButtonDiscription, positivButtonClick::action);
    }


    public PromtDialog(
            View view,
            String title,
            String positivButtonDiscription,
            String negativButtonDiscription,
            PositivButtonClick positivButtonClick,
            NegativButtonClick negativButtonClick
    )
    {
        dialog = new AlertDialog.Builder(App.getContext());
        dialog.setTitle(title);
        dialog.setView(view);
        dialog.setNegativeButton(negativButtonDiscription, negativButtonClick::action);
        dialog.setPositiveButton(positivButtonDiscription, positivButtonClick::action);
    }

    public PromtDialog(
            String title,
            String positivButtonDiscription,
            PositivButtonClick positivButtonClick,
            CancelButtonClick cancelButtonClick,
            View view
    )
    {
        dialog = new AlertDialog.Builder(App.getContext());
        dialog.setTitle(title);
        dialog.setView(view);
        dialog.setOnCancelListener(cancelButtonClick::action);
        dialog.setPositiveButton(positivButtonDiscription, positivButtonClick::action);

    }


    public AlertDialog.Builder getDialog() {
        return dialog;
    }

}
