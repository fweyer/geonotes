package de.fewycoding.android.geonotes;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;

public class App extends Application {

    @SuppressLint("StaticFieldLeak")
    private static Context mContext;

    public static Context getContext() {
        return mContext;
    }

    // Aktuell stellt diese Implementierung ein Memoryleak da,da die GatherActivity so nie aufgeräumt wird.
    // Sollte die Activity beendet werden (bei weiteren Erweiterungen) Muss diese Klasse umgebaut werden und
    // der Context global anders bereitgestellt werden.
    public static void setContext(Context mContext) {
        App.mContext = mContext;
    }

}
