package de.fewycoding.android.geonotes.screen.toast;

import android.annotation.SuppressLint;
import android.content.Context;
import android.widget.Toast;

import de.fewycoding.android.geonotes.R;

public class ToastHelper {

    private final Context context;

    public ToastHelper(Context context) {
        this.context = context;
    }

 // GatherActivity
    @SuppressLint("StringFormatMatches")
    public void lokalisierungNeuStarten(CharSequence title){Toast.makeText(context, context.getString(R.string.lokaliesirungNeuStarten, String.format("%s",title)), Toast.LENGTH_LONG).show();}
    public void themaUndNotizLeer(){ Toast.makeText(context, R.string.themaUndNotizLeer, Toast.LENGTH_SHORT).show(); }
    public void berechtigungAufGPS(){ Toast.makeText(context, R.string.berechtigungAufGPS, Toast.LENGTH_SHORT).show(); }
    public void notizGespeichert(){ Toast.makeText(context, R.string.notizGepeichert, Toast.LENGTH_SHORT).show(); }
    public void keineProjekteVorhanden(){ Toast.makeText(context, R.string.keineProjekteVorhanden, Toast.LENGTH_SHORT).show(); }
    public void keineGeoposition(){ Toast.makeText(context, R.string.keineGeoPosition, Toast.LENGTH_SHORT).show(); }
    public void notizGeloescht(){ Toast.makeText(context, R.string.notizGeloescht, Toast.LENGTH_SHORT).show();}
    public void bitteNotizVorherSpeichern(){Toast.makeText(context, R.string.bitteNotizVorherSpeichern, Toast.LENGTH_LONG).show();}
}
