package de.fewycoding.android.geonotes.database;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import de.fewycoding.android.geonotes.database.entity.Notiz;
import de.fewycoding.android.geonotes.database.entity.Projekt;

public class GeoNotesDatabaseHelper extends SQLiteOpenHelper {

    public static final String DB_NAME = "GeoNotes";
    public static final int DB_VERSION = 1;
    private final String CREATE_PROJEKTE = "CREATE TABLE IF NOT EXISTS " +
            "Projekte(id INTEGER PRIMARY KEY NOT NULL, beschreibung TEXT);";

    private final String CREATE_LOCATIONS = "CREATE TABLE IF NOT EXISTS " +
            "Locations(latitude REAL NOT NULL, " +
            "longitude REAL NOT NULL," +
            " altitude INTEGER, " +
            " provider TEXT NOT NULL, " +
            " PRIMARY KEY(latitude, longitude));";

    private final String CREATE_NOTIZEN = "CREATE TABLE IF NOT EXISTS " +
            "Notizen (id INTEGER PRIMARY KEY NOT NULL, " +
            "projekt INTEGER NOT NULL, " +
            "latitude REAL NOT NULL, " +
            "longitude REAL NOT NULL, " +
            "notiz TEXT NOT NULL, " +
            "thema TEXT, " +
            "data BLOB, " +
            "CONSTRAINT ProjektFK FOREIGN KEY(projekt) REFERENCES " +
            "Projekte(id) ON DELETE RESTRICT ON UPDATE CASCADE, " +
            "CONSTRAINT LocationFK FOREIGN KEY(latitude, longitude) " +
            "REFERENCES Locations(latitude, longitude));";


    public GeoNotesDatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION
        );
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL("PRAGMA foreign_keys=ON;");
            db.execSQL(CREATE_PROJEKTE);
            db.execSQL(CREATE_LOCATIONS);
            db.execSQL(CREATE_NOTIZEN);
            Log.d(getClass().getSimpleName(), "Datenbank erzeugt in" + db.getPath() + "");
        } catch (SQLException ex) {
            Log.e(getClass().getSimpleName(), "onCreate: " +
                    ex.toString());
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    public void insert(String table, ContentValues values) {
        SQLiteDatabase db = getWritableDatabase();
        try {
            db.insertOrThrow(table, null, values);
        } catch (SQLException ex) {
            Log.d(this.getClass().getSimpleName(), ex.toString());
        } finally {
            db.close();
        }
    }

    public void update(String table, ContentValues values) {
        SQLiteDatabase db = getWritableDatabase();
        db.update(table, values, "id=" + values.get("id"), null);
        db.close();
    }

    public List<Projekt> getProjekte() {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query("Projekte", new String[]{"*"}, null,
                null, null, null, "id", null);
        List<Projekt> result = new ArrayList<>();
        while (cursor.moveToNext()) {
            Projekt projekt = new Projekt(cursor);
            result.add(projekt);
        }
        cursor.close();
        db.close();
        return result;
    }

    public Notiz getLetzteNotiz(Projekt projekt) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query("Notizen", new String[]{"*"},
                "projekt='" + projekt.id + "'", null, null, null, "id DESC",
                "1");
        Notiz letzteNotiz = null;
        if (cursor.moveToNext()) {
            letzteNotiz = new Notiz(cursor);
        }
        cursor.close();
        db.close();
        return letzteNotiz;
    }

    @SuppressLint("DefaultLocale")
    public Notiz getVorherigeNotiz(Notiz notiz) {
        SQLiteDatabase db = getReadableDatabase();
        String selection = String.format("projekt = '%d' AND id < '%s'", notiz.projekt, notiz.id);
        Cursor cursor = db.query("Notizen", new String[]{"*"},
                selection, null, null, null, "id DESC", "1");
        Notiz vorherigeNotiz = null;
        if (cursor.moveToNext()) {
            vorherigeNotiz = new Notiz(cursor);
        }
        cursor.close();
        db.close();
        if (vorherigeNotiz == null){
            return notiz;
        }
        return vorherigeNotiz;
    }

    @SuppressLint("DefaultLocale")
    public Notiz getNaechsteNotiz(Notiz notiz) {
        SQLiteDatabase db = getReadableDatabase();
        String selection = String.format("projekt = '%d' AND id > '%s'", notiz.projekt, notiz.id);
        Cursor cursor = db.query("Notizen", new String[]{"*"},
                selection, null, null, null, "id ASC", "1");
        Notiz naechsteNotiz = null;
        if (cursor.moveToNext()) {
            naechsteNotiz = new Notiz(cursor);
        }
        cursor.close();
        db.close();
        return naechsteNotiz;
    }

    public ArrayList<Notiz> getNotizen(Projekt projekt) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query("Notizen", new String[]{"*"},
                "projekt='" + projekt.id + "'", null, null, null, "id", null);
        ArrayList<Notiz> notizen = new ArrayList<>();
        while (cursor.moveToNext()) {
            notizen.add(new Notiz(cursor));
        }
        cursor.close();
        db.close();
        return notizen;
    }

    public void deleteNotiz(String table, Notiz notiz) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DELETE FROM " + table + " WHERE " + "id=" + notiz.id + ";");
    }

    public void deleteProjekt(String table, Projekt projekt) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DELETE FROM " + table + " WHERE " + "id=" + projekt.id + ";");
    }
}
