package de.fewycoding.android.geonotes.screen.activity;

import static de.fewycoding.android.geonotes.App.getContext;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.util.Log;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import com.google.android.gms.common.util.CollectionUtils;
import java.util.HashMap;
import java.util.List;
import de.fewycoding.android.geonotes.App;
import de.fewycoding.android.geonotes.R;
import de.fewycoding.android.geonotes.common.PermissionHelper;
import de.fewycoding.android.geonotes.common.PermissionListener;
import de.fewycoding.android.geonotes.common.PermissionState;
import de.fewycoding.android.geonotes.database.entity.Notiz;
import de.fewycoding.android.geonotes.database.entity.Projekt;
import de.fewycoding.android.geonotes.screen.activity.common.NoteLocationListener;
import de.fewycoding.android.geonotes.screen.activity.common.SpinnerProviderItemSelectListener;
import de.fewycoding.android.geonotes.screen.dialog.Dialogfactory;
import de.fewycoding.android.geonotes.screen.dialog.common.CancelButtonClick;
import de.fewycoding.android.geonotes.screen.dialog.common.ItemClick;
import de.fewycoding.android.geonotes.screen.dialog.common.NegativButtonClick;
import de.fewycoding.android.geonotes.screen.dialog.common.PositivButtonClick;
import de.fewycoding.android.geonotes.screen.toast.ToastHelper;
import de.fewycoding.android.geonotes.util.Validation;
import de.fewycoding.android.geonotes.viewmodel.GatherViewModel;
import de.fewycoding.android.geonotes.viewmodel.common.ObserverView;
import de.fewycoding.android.geonotes.viewmodel.state.GatherState;

public class GatherActivity extends AppCompatActivity
        implements ObserverView<GatherState>, PermissionListener {

    private static final String LETZTES_PROJEKT = "LETZTES_PROJEKT";

    private final NoteLocationListener locationListener = new NoteLocationListener();
    private GatherViewModel viewModel;
    private PermissionHelper permissionHelper;
    private ToastHelper toastHelper;

    private Spinner spinner;
    private EditText editTextThema;
    private EditText editTextNotiz;
    private TextView projektTitel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gather);
        App.setContext(this);
        initUIElements();
        viewModel = GatherViewModel.getInstance();
        permissionHelper = new PermissionHelper(this);
        viewModel.registerObserver(this);
        permissionHelper.setListener(this);
        toastHelper = new ToastHelper(this);
        starteAnwendung();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        setSharedPreferencesLetztesProjektID();
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        locationManager.removeUpdates(locationListener);
        viewModel.unregisterObserver(this);
        permissionHelper.setListener(null);
        App.setContext(null);
    }

    private void initUIElements(){
        editTextThema =  findViewById(R.id.edittext_thema);
        editTextNotiz =  findViewById(R.id.edittext_notiz);
        projektTitel  =  findViewById(R.id.textview_aktuelles_projekt);
    }

    private void starteAnwendung() {
        initProjekt();
        setzeProvider();
    }

    private void setzeProvider(){
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        List<String> providers = locationManager.getProviders(true);
        Log.d(getClass().getSimpleName(), "Verfügbare Provider:");
        for (String provider : providers) {
            Log.d(getClass().getSimpleName(), "Provider: " + provider);
        }
        spinner = findViewById(R.id.spinner_provider);
        spinner.setAdapter(new ArrayAdapter<>(this, R.layout.list_item, providers));
        SpinnerProviderItemSelectListener itemSelectListener =
                new SpinnerProviderItemSelectListener(locationListener, viewModel.getMinTime(),viewModel.getMinDistance(), this);
        spinner.setOnItemSelectedListener(itemSelectListener);
    }


    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_gather_activity, menu);
        return true;
    }


    @SuppressLint("NonConstantResourceId")
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        HashMap<Integer, Pair<Integer, Integer>> idMap = new
                HashMap<Integer, Pair<Integer, Integer>>() {
                    {
                        put(R.id.item_foot, new Pair<>(9000, 10));
                        put(R.id.item_bicycle, new Pair<>(4000, 25));
                        put(R.id.item_car, new Pair<>(4000, 50));
                        put(R.id.item_car_fast, new Pair<>(4000, 10));
                    }
                };
        switch (id) {
            case R.id.item_foot:
            case R.id.item_bicycle:
            case R.id.item_car:
            case R.id.item_car_fast:
                item.setChecked(true);
                Pair<Integer, Integer> pair = idMap.get(id);
                if (pair == null) throw new RuntimeException();
                viewModel.setMinTime(pair.first);
                viewModel.setMinDistance(pair.second);
                toastHelper.lokalisierungNeuStarten(item.getTitle());
                break;
            case R.id.menu_projekt_anlegen:
                createProjektAnlegenDialog();
                onButtonNaechsteNotizClick(new View(this));
                break;
            case R.id.menu_projekt_bearbeiten:
                openProjektBearbeitenDialog();
                break;
            case R.id.menu_projekt_auswaehlen:
                openProjektAuswaehlenDialog();
                break;
            case R.id.menu_notiz_löschen:
                notizLoeschen();
            default:
                break;
        }
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressLint("SetTextI18n")
    private void openProjektAuswaehlenDialog() {
        List<Projekt> projekte = viewModel.getProjektList();
        if (CollectionUtils.isEmpty(projekte)) {
            toastHelper.keineProjekteVorhanden();
            return;
        }

        CancelButtonClick cancelButton = (dialog) -> openProjektAuswaehlenDialog();
        ItemClick<Projekt> itemClick = (dialog, which, items) -> viewModel.setAktuellesProjekt(items.get(which).id);
        Dialogfactory.getInstance()
                .createProjektAuswaelenDialog(projekte,cancelButton,itemClick)
                .show();
    }

    private void projektLoeschenClick(){
        PositivButtonClick positivButtonClick = (dialog, which) -> viewModel.deleteProjekt();
        Dialogfactory.getInstance()
                .createProjektLoeschenDialog(positivButtonClick)
                .show();
    }


    private void dialogErstelleProjekt() {
        PositivButtonClick positivButtonClick = (dialog, which)-> createProjektAnlegenDialog();
        NegativButtonClick negativButtonClick = (dialog, which)-> finish();
        CancelButtonClick  cancelButtonClick  = (dialog) -> viewModel.checkAktuellProjekt();
        Dialogfactory.getInstance()
                .createErstelleProjektDialog(positivButtonClick,negativButtonClick,cancelButtonClick)
                .show();
    }

    @SuppressLint("InflateParams")
    private void createProjektAnlegenDialog() {
        View dialogView = getLayoutInflater().inflate(R.layout.dialog_projekt_bearbeiten, null);
        CancelButtonClick  cancelButtonClick = (dialog)-> viewModel.checkAktuellProjekt();
        PositivButtonClick positivButtonClick = (dialog, which)-> {
            EditText editProjektbeschreibung = dialogView.findViewById(R.id.edittext_dialog_projekt_bearbeiten);
            String beschreibung = editProjektbeschreibung.getText().toString();
            projektSpeichern(beschreibung, false);
        };
        Dialogfactory.getInstance()
                .createProjektAnlegenDialog(dialogView,positivButtonClick, cancelButtonClick)
                .show();
    }

    @SuppressLint("InflateParams")
    private void projektSpeichern(String beschreibung, boolean updateProjekt) {
        if ("".equals(beschreibung.trim())) {
            View dialogView = getLayoutInflater().inflate(R.layout.dialog_projekt_bearbeiten, null);
            EditText editText = dialogView.findViewById(R.id.edittext_dialog_projekt_bearbeiten);
            Editable neueBeschreibung = editText.getText();
            PositivButtonClick positivButtonClick = (dialog, which)->  projektSpeichern(neueBeschreibung.toString(), updateProjekt);
            Dialogfactory.getInstance()
                    .keineLeereProjektBeschreibungDialog(dialogView,positivButtonClick)
                    .show();
        } else {
            viewModel.projektSpeichern(beschreibung,updateProjekt);
        }
    }

    @SuppressLint({"InflateParams", "CutPasteId"})
    private void openProjektBearbeitenDialog() {
        View dialogView = getLayoutInflater().inflate(R.layout.dialog_projekt_bearbeiten, null);
        TextView textView = dialogView.findViewById(R.id.textview_dialog_projekt_bearbeiten);
        textView.setText(viewModel.getAktuellesProjekt().toString());
        EditText editText = dialogView.findViewById(R.id.edittext_dialog_projekt_bearbeiten);
        editText.setText(viewModel.getAktuellesProjekt().getBeschreibung());

        PositivButtonClick positivButtonClick = (dialog, which) -> {
            TextView viewbeschreibung = dialogView.findViewById(R.id.edittext_dialog_projekt_bearbeiten);
            String beschreibung = viewbeschreibung.getText().toString().trim();
            Log.i(getClass().getSimpleName(), beschreibung);
            projektSpeichern(beschreibung, true);
        };
        Dialogfactory.getInstance()
                .createProjektBearbeitenDialog(dialogView, positivButtonClick)
                .show();
    }

    public void notizSpeichern(View view) {
        String thema = editTextThema.getText().toString().trim();
        String notiz = editTextNotiz.getText().toString().trim();
        if (!Validation.istNotizValide(notiz,thema)){
            toastHelper.themaUndNotizLeer();
            return;
        }
        String provider = (String) spinner.getSelectedItem();
        if (!permissionHelper.hasGPSPermission()) return;
        if (provider == null) {
            setzeProvider();
            provider = (String) spinner.getSelectedItem();
        }
        Location lastLocation = getLastLocation(provider);
        viewModel.notizSpeichern(thema,notiz,lastLocation,provider);
        toastHelper.notizGespeichert();
    }

    public void notizLoeschen() {
        boolean notizGeloescht = viewModel.deleteAktuelleNotiz();
        if (notizGeloescht)  toastHelper.notizGeloescht();
        if(viewModel.isNotizListeLeer()){
            projektLoeschenClick();
        }
    }

    public void onButtonVorherigeNotizClick(View view) {
        viewModel.voherigeNotiz();
    }

    public void onButtonNaechsteNotizClick(View view) { viewModel.nachsteNotiz(); }


    private Location getLastLocation(String provider){
        try {
            LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            Location lastLocation = locationManager.getLastKnownLocation(provider);
            if (lastLocation == null) {
                toastHelper.keineGeoposition();
                return null;
            }
            return lastLocation;

        } catch (SecurityException ex) {
            Log.e(getClass().getSimpleName(), "Erforderliche Berechtigung fehlt " + ex.toString());
            return null;
        }
    }


    public void onToggleButtonLokalisierungClick(View view) {
        if (!permissionHelper.hasGPSPermission()) return;
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (((ToggleButton) view).isChecked()) {
            try {
                String provider = (String) spinner.getSelectedItem();
                locationManager.requestLocationUpdates(provider, viewModel.getMinTime(),
                        viewModel.getMinDistance(),
                        locationListener);
                Log.d(getClass().getSimpleName(), "Lokalisierung gestartet");
            } catch (SecurityException ex) {
                Log.e(getClass().getSimpleName(), "Erforderliche Berechtigung fehlt: " + ex.toString());
            }
        } else {
            locationManager.removeUpdates(locationListener);
            Log.d(getClass().getSimpleName(), "Lokalisierung beendet");
        }
    }

    public void onClickStandortAnzeigen(View view) {
        if(CollectionUtils.isEmpty(viewModel.getAktuelleNotizList())){
            toastHelper.bitteNotizVorherSpeichern();
            return;
        }

        if (viewModel.getAktuelleNotiz() == null){
            int size = viewModel.getAktuelleNotizList().size();
            Notiz notiz = viewModel.getAktuelleNotizList().get(size-1);
            NoteMapActivity.start(this, viewModel.getAktuellesProjekt().id, notiz);
        } else {
            NoteMapActivity.start(this, viewModel.getAktuellesProjekt().id, viewModel.getAktuelleNotiz());
        }
    }

    //Wenn das ViewModel seinen State verändert wird diese Methode aufgerufen
    @Override
    public void update(GatherState state) {
        switch (state){
            case AKTUELLES_PROJEKT_NULL:
                removeSharedPreferencesLetztesProjektID();
                initProjekt();
                break;
            case AKTUELLES_PROJEKT_VERAENDERT:
                setzteProjektTitel();
                break;
            case AKTUELLES_NOTIZ_VERAENDERT:
                notizWechseln();
                break;
            case AKTUELLE_NOTIZ_NULL:
                notizFelderLeeren();
                break;
            default:
                break;
        }
    }

    private void initProjekt(){
        if (viewModel.isProjektListeLeer()){
            dialogErstelleProjekt();
            return;
        }

        if (getSharedPreferencesLetztesProjektID() != 0)
            viewModel.setAktuellesProjekt(getSharedPreferencesLetztesProjektID());
        else
            openProjektAuswaehlenDialog();
    }

    @SuppressLint("SetTextI18n")
    private void setzteProjektTitel(){
       projektTitel.setText("Aktuelles Projekt: " + viewModel.getAktuellenProjektTitel());
    }

    private void notizFelderLeeren(){
       editTextThema.setText("");
       editTextNotiz.setText("");
    }

    private void notizWechseln(){
        editTextThema.setText(viewModel.getAktuelleNotiz().getThema());
        editTextNotiz.setText(viewModel.getAktuelleNotiz().getNotiz());
    }

    private void setSharedPreferencesLetztesProjektID(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        SharedPreferences.Editor editor = prefs.edit();
        editor.putLong(LETZTES_PROJEKT, viewModel.getAktuellesProjekt().id);
        editor.apply();
    }

    private long getSharedPreferencesLetztesProjektID(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        return (prefs.getLong(LETZTES_PROJEKT, 0));
    }

    private void removeSharedPreferencesLetztesProjektID(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        prefs.edit().remove(LETZTES_PROJEKT).commit();
    }

    @Override
    public void execution(PermissionState state, String permission, int requestCode) {
        if(state == PermissionState.PERMISSION_DONT_ASK_AGAIN){
            toastHelper.berechtigungAufGPS();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        permissionHelper.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}


