package de.fewycoding.android.geonotes.viewmodel.common;


public interface ObserverView<UPDATE_EVENT> {

    void update(UPDATE_EVENT state);

}
