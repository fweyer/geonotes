package de.fewycoding.android.geonotes.common;

public interface PermissionListener {

    void execution (PermissionState state, String permission, int requestCode);

}
