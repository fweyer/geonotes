package de.fewycoding.android.geonotes.screen.dialog.components;

import android.app.AlertDialog;
import android.view.View;

import de.fewycoding.android.geonotes.App;
import de.fewycoding.android.geonotes.screen.dialog.common.PositivButtonClick;

public class InfoDialog {

    AlertDialog.Builder dialog;

    public InfoDialog(
            String title,
            String positivButtonDiscription,
            PositivButtonClick positivButtonClick,
            View view
    )
    {
        dialog = new AlertDialog.Builder(App.getContext());
        dialog.setTitle(title);
        dialog.setView(view);
        if (positivButtonClick != null){
            dialog.setPositiveButton(positivButtonDiscription, positivButtonClick::action);
        }
    }

    public AlertDialog.Builder getDialog() {
        return dialog;
    }

}
