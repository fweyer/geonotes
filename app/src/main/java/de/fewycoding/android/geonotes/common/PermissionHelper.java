package de.fewycoding.android.geonotes.common;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;


public class PermissionHelper {

    private final Activity activity;
    private PermissionListener listener;

    public PermissionHelper(Activity activity) {
        this.activity = activity;
    }

    public boolean hasPermission(String permission) {
        return ContextCompat.checkSelfPermission(activity, permission) == PackageManager.PERMISSION_GRANTED;
    }

    public void requestPermission(String permission, int requestCode) {
        ActivityCompat.requestPermissions(activity, new String[]{ permission }, requestCode);
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (permissions.length < 1) {
            throw new RuntimeException("no permissions on request result");
        }
        String permission = permissions[0];
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            notifyPermissionGranted(permission, requestCode);
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
                notifyPermissionDeclined(permission, requestCode);
            } else {
                notifyPermissionDeclinedDontAskAgain(permission, requestCode);
            }
        }
    }

    public void setListener(PermissionListener listener) {
        this.listener = listener;
    }


    private void notifyPermissionGranted(String permission, int requestCode) {
        listener.execution(PermissionState.GRANTED, permission, requestCode);
    }

    private void notifyPermissionDeclined(String permission, int requestCode) {
        listener.execution(PermissionState.PERMISSION_DECLINED, permission, requestCode);
    }

    private void notifyPermissionDeclinedDontAskAgain(String permission, int requestCode) {
        listener.execution(PermissionState.PERMISSION_DONT_ASK_AGAIN, permission, requestCode);
    }


    public boolean hasGPSPermission(){
        if (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION)) {
            return true;
        } else {
            requestPermission(Manifest.permission.ACCESS_FINE_LOCATION, 1001);
            return false;
        }
    }

}
