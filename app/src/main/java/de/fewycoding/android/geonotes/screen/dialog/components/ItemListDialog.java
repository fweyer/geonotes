package de.fewycoding.android.geonotes.screen.dialog.components;

import android.app.AlertDialog;
import android.content.DialogInterface;

import java.util.ArrayList;
import java.util.List;

import de.fewycoding.android.geonotes.App;
import de.fewycoding.android.geonotes.screen.dialog.common.CancelButtonClick;
import de.fewycoding.android.geonotes.screen.dialog.common.ItemClick;
import de.fewycoding.android.geonotes.screen.dialog.common.ItemDialogElement;

public class ItemListDialog<E> {

    AlertDialog.Builder dialog;

    public <E extends ItemDialogElement> ItemListDialog(
            String title,
            List<E> elements,
            CancelButtonClick cancelButtonClick,
            ItemClick itemClick
    )
    {
        dialog = new AlertDialog.Builder(App.getContext());
        dialog.setTitle(title);
        List<String> items = new ArrayList<>();
        for (E element : elements) { items.add(element.getDiscription()); }
        int size = items.size();
        dialog.setItems(
                items.toArray(new CharSequence[size]), (DialogInterface dialog, int which) ->
            {
                itemClick.action(dialog,which, elements);
            });
        if (cancelButtonClick != null){
            dialog.setOnCancelListener(cancelButtonClick::action);
        }
    }

    public AlertDialog.Builder getDialog() {
        return dialog;
    }
}
