package de.fewycoding.android.geonotes.viewmodel;

import android.util.Log;

import java.util.List;

import de.fewycoding.android.geonotes.database.entity.Notiz;
import de.fewycoding.android.geonotes.database.entity.Projekt;
import de.fewycoding.android.geonotes.repository.GeoNotesRepository;
import de.fewycoding.android.geonotes.viewmodel.common.ObservableModel;
import de.fewycoding.android.geonotes.viewmodel.state.NoteMapState;

public class NoteMapViewModel extends ObservableModel<NoteMapState> {

    private static NoteMapViewModel instance;
    private final GeoNotesRepository repository;

    private List<Notiz> notizList;
    private int currentIndex;

    private NoteMapViewModel(){
        this.repository = GeoNotesRepository.getInstance();
    }

    public static NoteMapViewModel getInstance(){
        if (instance == null){
            instance = new NoteMapViewModel();
            instance.setMemberObserverAllowed(1);
        }
        return instance;
    }

    public void setNotizList(long projektID){
        Projekt projekt = repository.findProjektByID(projektID);
        this.notizList = repository.getNotizen(projekt);
    }

    public List<Notiz> getNotizList() {
        return notizList;
    }

    public void setCurrentIndex(Notiz notiz){
        this.currentIndex = notizList.indexOf(notiz);
    }

    public void dekrementNotizIndex(){
        if (currentIndex <= 0){
            currentIndex = notizList.size() - 1 ;
        } else {
            currentIndex--;
        }
        Log.d(getClass().getSimpleName(), "Index gleich " + currentIndex );
        notifyUpdates(NoteMapState.CURRENT_INDEX_CHANGED);
    }

    public void inkrementNotizIndex(){
        if (currentIndex >= notizList.size()-1){
            currentIndex = 0;
        } else {
            currentIndex++;
        }
        notifyUpdates(NoteMapState.CURRENT_INDEX_CHANGED);
    }

    public Notiz getCurrentNotiz(){
        return notizList.get(currentIndex);
    }



}
