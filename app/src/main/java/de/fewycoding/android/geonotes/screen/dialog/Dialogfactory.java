package de.fewycoding.android.geonotes.screen.dialog;

import android.app.AlertDialog;
import android.view.View;

import java.util.List;

import de.fewycoding.android.geonotes.database.entity.Projekt;
import de.fewycoding.android.geonotes.screen.dialog.common.CancelButtonClick;
import de.fewycoding.android.geonotes.screen.dialog.common.ItemClick;
import de.fewycoding.android.geonotes.screen.dialog.common.NegativButtonClick;
import de.fewycoding.android.geonotes.screen.dialog.common.PositivButtonClick;
import de.fewycoding.android.geonotes.screen.dialog.components.InfoDialog;
import de.fewycoding.android.geonotes.screen.dialog.components.ItemListDialog;
import de.fewycoding.android.geonotes.screen.dialog.components.PromtDialog;
import de.fewycoding.android.geonotes.screen.dialog.components.ThreeButtonDialog;

public class Dialogfactory {

    private static Dialogfactory instance;

    private Dialogfactory(){}

    public static Dialogfactory getInstance(){
        if (instance == null){
            instance = new Dialogfactory();
        }
        return instance;
    }

    public AlertDialog.Builder createProjektLoeschenDialog( PositivButtonClick positivButtonClick){
        return new PromtDialog(
                "Projekt löschen?",
                "Ja",
                "Nein",
                positivButtonClick,
                getFunktionslosenNegativButton()
        ).getDialog();
    }

    public AlertDialog.Builder createErstelleProjektDialog(
            PositivButtonClick positivButtonClick,
            NegativButtonClick negativButtonClick,
            CancelButtonClick cancelButtonClick
    ){
        return new ThreeButtonDialog(
                "Bitte legen Sie ein Projekt an",
                "Projekt anlegen",
                "App schließen",
                positivButtonClick,
                negativButtonClick,
                cancelButtonClick
        ).getDialog();
    }

    public AlertDialog.Builder createProjektAnlegenDialog(
            View view,
            PositivButtonClick positivButtonClick,
            CancelButtonClick cancelButtonClick
    ){
        return new PromtDialog(
                "Projektbeschreibung einfügen",
                "Bestätigen",
                positivButtonClick,
                cancelButtonClick,
                view
        ).getDialog();
    }


    public AlertDialog.Builder createProjektAuswaelenDialog(
            List<Projekt> projekte,
            CancelButtonClick cancelButtonClick,
            ItemClick itemClick
    ){
        return new ItemListDialog(
                "Projekt auswählen",
                projekte,
                cancelButtonClick,
                itemClick
        ).getDialog();
    }

    public AlertDialog.Builder createProjektBearbeitenDialog(
            View view,
            PositivButtonClick positivButtonClick
    ){
        return new PromtDialog(
                view,
                "Projektbeschreibung eingeben / ändern",
                "Übernehmen",
                "Abbrechen",
                positivButtonClick,
                getFunktionslosenNegativButton()
        ).getDialog();
    }

    public AlertDialog.Builder keineLeereProjektBeschreibungDialog(
            View view,
            PositivButtonClick positivButtonClick
    ){
        return new InfoDialog(
                "Bitte keine leere Beschreibung angeben",
                "Bestätigen",
                positivButtonClick,
                view
        ).getDialog();
    }

    public NegativButtonClick getFunktionslosenNegativButton(){
        return (dialog, which) -> { };
    }


}
