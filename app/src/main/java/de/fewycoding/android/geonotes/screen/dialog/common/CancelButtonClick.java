package de.fewycoding.android.geonotes.screen.dialog.common;

import android.content.DialogInterface;

public interface CancelButtonClick {

    void action(DialogInterface dialog);
}
