package de.fewycoding.android.geonotes.screen.dialog.components;

import android.app.AlertDialog;

import de.fewycoding.android.geonotes.App;
import de.fewycoding.android.geonotes.screen.dialog.common.CancelButtonClick;
import de.fewycoding.android.geonotes.screen.dialog.common.NegativButtonClick;
import de.fewycoding.android.geonotes.screen.dialog.common.PositivButtonClick;

public class ThreeButtonDialog {


    AlertDialog.Builder dialog;

    public ThreeButtonDialog(
            String title,
            String positivButtonDiscription,
            String negativButtonDiscription,
            PositivButtonClick positivButtonClick,
            NegativButtonClick negativButtonClick,
            CancelButtonClick cancelButtonClick
    )
    {
        dialog = new AlertDialog.Builder(App.getContext());
        dialog.setTitle(title);
        if (negativButtonClick != null){
            dialog.setNegativeButton(negativButtonDiscription, negativButtonClick::action);
        }
        if (positivButtonClick != null){
            dialog.setPositiveButton(positivButtonDiscription, positivButtonClick::action);
        }
        if (cancelButtonClick != null){
            dialog.setOnCancelListener(cancelButtonClick::action);
        }

    }

    public AlertDialog.Builder getDialog() {
        return dialog;
    }

}
