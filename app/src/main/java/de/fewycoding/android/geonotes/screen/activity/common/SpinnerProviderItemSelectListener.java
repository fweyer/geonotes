package de.fewycoding.android.geonotes.screen.activity.common;

import static android.content.Context.LOCATION_SERVICE;

import android.app.Activity;
import android.location.LocationManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.ToggleButton;
import de.fewycoding.android.geonotes.App;
import de.fewycoding.android.geonotes.R;

public class SpinnerProviderItemSelectListener implements AdapterView.OnItemSelectedListener {

    private final NoteLocationListener locationListener;
    private final int minTime;
    private final int minDistance;
    private final ToggleButton toggleButton;

    public SpinnerProviderItemSelectListener(NoteLocationListener locationListener, int minTime, int minDistance, Activity activity) {
        this.locationListener = locationListener;
        this.minTime = minTime;
        this.minDistance = minDistance;
        toggleButton = activity.findViewById(R.id.togglebutton_lokalisierung);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        if (toggleButton.isChecked()) {
            LocationManager locationManager = (LocationManager) App.getContext().getSystemService(LOCATION_SERVICE);
            locationManager.removeUpdates(locationListener);
            String provider = ((TextView) view).getText().toString();
            try {
                locationManager.requestLocationUpdates(provider, minTime,
                       minDistance, locationListener);
            } catch (SecurityException ex) {
                Log.e(getClass().getSimpleName(), "Erforderliche Berechtigung fehlt: "
                        + ex.toString());
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) { }
}
