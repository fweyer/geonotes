package de.fewycoding.android.geonotes.screen.dialog.common;

import android.content.DialogInterface;

public interface NegativButtonClick {

    void action(DialogInterface dialog, int which);
}
