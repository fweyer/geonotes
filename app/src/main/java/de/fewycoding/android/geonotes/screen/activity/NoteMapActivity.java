package de.fewycoding.android.geonotes.screen.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import de.fewycoding.android.geonotes.R;
import de.fewycoding.android.geonotes.database.entity.Notiz;
import de.fewycoding.android.geonotes.screen.activity.components.MarkerInfoWindow;
import de.fewycoding.android.geonotes.viewmodel.NoteMapViewModel;
import de.fewycoding.android.geonotes.viewmodel.common.ObserverView;
import de.fewycoding.android.geonotes.viewmodel.state.NoteMapState;

public class NoteMapActivity extends AppCompatActivity implements ObserverView<NoteMapState> {

    private NoteMapViewModel viewModel;
    private GoogleMap googleMap;

    public static void start(Context context, long projektID, Notiz notiz) {
        Intent intent = new Intent(context, NoteMapActivity.class);
        context.startActivity(intent);
        NoteMapViewModel.getInstance().setNotizList(projektID);
        NoteMapViewModel.getInstance().setCurrentIndex(notiz);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_map);
        viewModel = NoteMapViewModel.getInstance();
        initNotemap();
    }

    @Override
    protected void onStart() {
        super.onStart();
        viewModel.registerObserver(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        viewModel.unregisterObserver(this);
    }

    private void initNotemap(){
        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.mapfragment);
        mapFragment.getMapAsync(googleMap -> {
            NoteMapActivity.this.googleMap = googleMap;
            googleMap.setInfoWindowAdapter(new MarkerInfoWindow(this.getLayoutInflater()));
            setzteMarker();
            setztePositionAufMaps(viewModel.getCurrentNotiz(), 17.0f);
        });
    }

    private void setzteMarker(){
        for (Notiz notiz : viewModel.getNotizList()) {
            LatLng position = new LatLng(notiz.latitude, notiz.longitude);
            String title = notiz.getThema();
            String snippet = notiz.getNotiz();
            MarkerOptions options = new MarkerOptions().position(position).title(title).snippet(snippet);
            googleMap.addMarker(options);
        }
    }

    public void onButtonVorherigeNotizClick(View view) {
        viewModel.dekrementNotizIndex();
    }

    public void onButtonNaechsteNotizClick(View view) {
        viewModel.inkrementNotizIndex();
    }

    public void setztePositionAufMaps(Notiz notiz, float v) {
        if (notiz != null){
            LatLng position = new LatLng(notiz.latitude, notiz.longitude);
            CameraPosition cameraPosition = CameraPosition.fromLatLngZoom(position, v);
            CameraUpdate update = CameraUpdateFactory.newCameraPosition(cameraPosition);
            googleMap.moveCamera(update);
        }
    }

    @Override
    public void update(NoteMapState state) {
        if (state == NoteMapState.CURRENT_INDEX_CHANGED) {
            Notiz notiz = viewModel.getCurrentNotiz();
            setztePositionAufMaps(notiz, googleMap.getCameraPosition().zoom);
        }
    }
}
