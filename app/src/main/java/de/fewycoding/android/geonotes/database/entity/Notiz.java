package de.fewycoding.android.geonotes.database.entity;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.Date;

public  class Notiz implements Parcelable {

    public final long id;
    public final long projekt;
    public final double latitude;
    public final double longitude;
    private String thema;
    private String notiz;
    private final byte[] data;

    public static final Parcelable.Creator<Notiz> CREATOR = new Creator<Notiz>() {
        @Override
        public Notiz createFromParcel(Parcel parcel) {
            return new Notiz(parcel);
        }

        @Override
        public Notiz[] newArray(int size) {
            return new Notiz[size];
        }
    };


    public Notiz(long id, long projekt, double latitude, double longitude, String thema, String notiz, byte[] data) {
        this.id = id;
        this.projekt = projekt;
        this.longitude = longitude;
        this.latitude = latitude;
        this.thema = thema;
        this.notiz = notiz;
        this.data = data;
    }

    public Notiz(Parcel parcel) {
        this(parcel.readLong(),
                parcel.readLong(),
                parcel.readDouble(),
                parcel.readDouble(),
                parcel.readString(),
                parcel.readString(),
                parcel.createByteArray());
    }


    public Notiz(long projekt, double latitude, double longitude, String thema, String notiz) {
        this(new Date().getTime(), projekt, latitude, longitude, thema, notiz, null);
    }

    public Notiz(Cursor cursor) {
        this(cursor.getLong(cursor.getColumnIndex("id")),
                cursor.getLong(cursor.getColumnIndex("projekt")),
                cursor.getDouble(cursor.getColumnIndex("latitude")),
                cursor.getDouble(cursor.getColumnIndex("longitude")),
                cursor.getString(cursor.getColumnIndex("thema")),
                cursor.getString(cursor.getColumnIndex("notiz")),
                cursor.getBlob(cursor.getColumnIndex("data")));
    }

    public String getThema() {
        return thema;
    }
    public String getNotiz() {
        return notiz;
    }



    public ContentValues getContentValues() {
        ContentValues values = new ContentValues(7);
        values.put("id", id);
        values.put("projekt", projekt);
        values.put("latitude", latitude);
        values.put("longitude", longitude);
        values.put("thema", thema);
        values.put("notiz", notiz);
        values.put("data", data);

        return values;
    }
    public void setThema(String thema) {
        this.thema = thema;
    }
    public void setNotiz(String notiz) {
        this.notiz = notiz;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int i) {
        dest.writeLong(id);
        dest.writeLong(projekt);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
        dest.writeString(thema);
        dest.writeString(notiz);
        dest.writeByteArray(data);
    }
    @Override
    public boolean equals(Object other) {
        if (other instanceof Notiz)
            return ((Notiz) other).id == id;
        return false;
    }

}