package de.fewycoding.android.geonotes.screen.activity.components;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import de.fewycoding.android.geonotes.R;

public class MarkerInfoWindow implements GoogleMap.InfoWindowAdapter{

    private final LayoutInflater layoutInflater;

    public MarkerInfoWindow(LayoutInflater layoutInflater){
        this.layoutInflater = layoutInflater;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getInfoContents(Marker marker) {
        View popupWindow = layoutInflater.inflate(R.layout.popup_window_layout, null);
        TextView title = popupWindow.findViewById(R.id.textview_popup_titel);
        title.setText(marker.getTitle());
        TextView snippet = popupWindow.findViewById(R.id.textview_popup_snippet);
        snippet.setText(marker.getSnippet());
        return popupWindow;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }
}
