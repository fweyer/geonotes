package de.fewycoding.android.geonotes.screen.dialog.common;

import android.content.DialogInterface;

public interface PositivButtonClick {

    void action(DialogInterface dialog, int which);

}
